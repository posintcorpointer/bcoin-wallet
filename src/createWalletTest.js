"use strict";

const assert = require("bsert");
const { WalletClient } = require("../lib/client");
const consensus = require("../lib/protocol/consensus");
const util = require("../lib/utils/util");
const hash256 = require("bcrypto/lib/hash256");
const random = require("bcrypto/lib/random");
const FullNode = require("../lib/node/fullnode");
const WalletDB = require("../lib/wallet/walletdb");
const WorkerPool = require("../lib/workers/workerpool");
const Address = require("../lib/primitives/address");
const MTX = require("../lib/primitives/mtx");
const Coin = require("../lib/primitives/coin");
const KeyRing = require("../lib/primitives/keyring");
const Input = require("../lib/primitives/input");
const Outpoint = require("../lib/primitives/outpoint");
const Script = require("../lib/script/script");
const HD = require("../lib/hd");
const Wallet = require("../lib/wallet/wallet");
const nodejsUtil = require("util");
const HDPrivateKey = require("../lib/hd/private");
const policy = require("../lib/protocol/policy");
const { forValue } = require("./util/common");

walletExample().catch(console.error.bind(console));

async function walletExample() {
    const wdb = new WalletDB({ db: "memory" });
    await wdb.open();

    const wallet = await wdb.create({witness: false});

    const xpriv = wallet.master.key;
    const xprivkey = xpriv.deriveAccount(44, 0, 0);
    const xpubkey = xprivkey.derivePath("m/0/0").xpubkey("main");
    const account = await wallet.getAccount(0);
    const accountkey = await wallet.createReceive(0);
    const address = accountkey.getAddress('string', 'main');

    //console.log(wallet.master.toJSON());
	//console.log(xpriv);
	//console.log(xprivkey);
	console.log(account);
    console.log(accountkey);
    //console.log(address);
    //console.log(xpubkey);
}