const assert = require("bsert");
const createWallet = require("../../src/wallet");

async function walletCreate() {
    const wallet = new createWallet();
    return await wallet.create();
}

walletCreate()
    .then((result) => console.log(result))
    .catch((error) => console.log("Error: ", error));
