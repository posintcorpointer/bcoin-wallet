"use strict";

const WalletDB = require("../../lib/wallet/walletdb");

class createWallet {
    constructor() {}

    randomString(length = 10) {
        const name = "IBLO";
        const chars = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        let result = "";
        for (let i = length; i > 0; --i) result += chars[Math.floor(Math.random() * chars.length)];
        return name + result;
    }

    async create() {
        try {
            const wdb = new WalletDB({ db: "memory" });
            await wdb.open();
            const wallet = await wdb.create({ id: this.randomString() + Math.floor(Date.now() * 1000).toString(), witness: false, watchOnly: false });
            const xpriv = wallet.master.key;
            const xprivkey = xpriv.deriveAccount(44, 0, 0);
            const xpubkey = xprivkey.derivePath("m/0/0").xpubkey("main");
            const account = await wallet.getAccount(0);
            const accountkey = await wallet.createReceive(0);
            const address = accountkey.getAddress("string", "main");

            const result = {
                wallet: {
                    id: wallet.id,
                    privatekey: xpriv.toJSON(),
                    seedphrases: wallet.master.mnemonic.phrase,
                },
                account: {
                    name: account.name,
                    network: account.network.type,
                    initialized: account.initialized,
                    witness: account.witness,
                    watchOnly: account.watchOnly,
                    accountKey: account.accountKey.toJSON(),
                },
                publickey: xpubkey,
            };
            return result;
        } finally {
        }
    }
}
module.exports = createWallet;
